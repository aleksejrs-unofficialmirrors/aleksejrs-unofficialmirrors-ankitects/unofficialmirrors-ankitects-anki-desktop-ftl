about-a-big-thanks-to-all-the = Baie dankie aan al die mense wat voorstelle, foutverslae en skenkings aangestuur het.
about-about-anki = Oor Anki
about-anki-is-a-friendly-intelligent-spaced = Anki is 'n vriendelike, intelligente gespasieerde leer stelsel. Dit is gratis en openbare bron.
about-anki-is-licensed-under-the-agpl3 = Anki is gelisensieer met die AGPL3 lisensie. Sien asseblief die lisensie lêer in die bron verspreiding vir meer inligting.
about-if-you-have-contributed-and-are = As jy bygedra het, en nie op hierdie lys is nie, tree asseblief in verbinding.
about-version = Weergawe { $val }
about-visit-website = <a href='{ $val }'>Besoek webwerf</a>
