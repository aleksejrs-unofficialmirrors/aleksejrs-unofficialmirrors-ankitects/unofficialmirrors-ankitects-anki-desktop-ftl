errors-accessing-db =
    Daar was n probleem met toegang tot die databasis.
    
    Moontlike oorsake:
    
    - Antivirus, brandmuur, rugsteun, of sinchronisasie sagteware belemmer Anki. Probeer diaardie sagteware deaktiveer en kyk of die probleem opgelos is.
    - Jou hardeskyf is dalk vol.
    - Die Documents/Anki lêer is dalk geleë op n netwerk skyf.
    - Lêers in die  Documents/Anki omslag mag dalk nie na toe geskryf kan word nie.
    - Jou harde skyf mag foutief wees.
    
    Dit is n goeie idee om Nutsprogramme>Ondersoek Databasis om te verseker jou databasis is nie korrup nie.

