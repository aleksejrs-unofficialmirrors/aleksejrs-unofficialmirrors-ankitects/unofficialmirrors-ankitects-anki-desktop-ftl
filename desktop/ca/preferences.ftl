# shown instead of the 'night mode' option when night mode is forced on because
# macOS is in dark mode
preferences-dark-mode-active = macOS està en mode nocturn
preferences-dark-mode-disable =
    Per mostrar Anki en mode diürn mentre macOS està en mode nocturn, si us plau
    llegeixi la secció de l'manual sobre Mode Nocturn.
